from django.test import TestCase, Client
from django.contrib.auth.models import User
from .forms import RegisterForm

# Create your tests here.
class TestStory9(TestCase):
    def test_if_sign_in_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_if_content_sign_in_is_exist(self):
        response = Client().get('/')
        html_response = response.content.decode('utf-8')
        self.assertIn('Welcome Back', html_response)

    def test_if_sign_up_url_is_exist(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)

    def test_if_content_sign_up_is_exist(self):
        response = Client().get('/register/')
        html_response = response.content.decode('utf-8')
        self.assertIn('Create Account', html_response)

    def test_register_someone_success(self):
        form_data_user = {
            'username' : 'raditya',
            'password1' : 'inipasswordnya',
            'password2' : 'inipasswordnya',
        }

        response = Client().post('/register/', data = form_data_user)
        html_response = response.content.decode('utf-8')
        self.assertEqual(response.status_code, 200)
        self.assertIn('Congratulations, your account has been successfully created! Please Kindly Signin on the Link Above!', html_response)

    def test_register_someone_failed(self):
        form_data_user = {
            'username' : 'raditya',
            'password1' : 'inipasswordnya',
            'password2' : 'inipasswordnyaa',
        }

        response = Client().post('/register/', data = form_data_user)
        html_response = response.content.decode('utf-8')
        self.assertIn('*user registration failed', html_response)

    def test_log_in_someone_success(self):
        form_data_user = {
            'username' : 'raditya',
            'password1' : 'inipasswordnya',
            'password2' : 'inipasswordnya',
        }

        Client().post('/register/', data = form_data_user)

        user_login = {
            'username' : 'raditya',
            'password' : 'inipasswordnya',
        }

        response = Client().post('/', user_login)

        html_response = response.content.decode('utf-8')
        self.assertIn('Hallo', html_response)
        self.assertIn('raditya !', html_response)

    def test_log_in_someone_failed(self):
        form_data_user = {
            'username' : 'raditya',
            'password1' : 'inipasswordnya',
            'password2' : 'inipasswordnya',
        }

        Client().post('/register/', data = form_data_user)

        user_login = {
            'username' : 'raditya',
            'password' : 'inipasswordnyaa',
        }

        response = Client().post('/', user_login)

        html_response = response.content.decode('utf-8')
        self.assertIn('*username or password not correct', html_response)

    def test_if_landing_url_is_exist(self):
        response = Client().get('/landing/')
        self.assertEqual(response.status_code, 200)

    def test_ajax_request(self):
        response_ajax = Client().post('/landing/getData/', {'bookName' : 'java'})
        self.assertEqual(response_ajax.status_code, 200)

    def test_log_out(self):
        form_data_user = {
            'username' : 'raditya',
            'password1' : 'inipasswordnya',
            'password2' : 'inipasswordnya',
        }

        Client().post('/register/', data = form_data_user)

        user_login = {
            'username' : 'raditya',
            'password' : 'inipasswordnya',
        }

        Client().post('/', user_login)

        response = Client().get('/logout/')

        self.assertRedirects(response, '/', status_code=302, 
        target_status_code=200, fetch_redirect_response=True)
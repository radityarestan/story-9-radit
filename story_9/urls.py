from django.urls import path
from . import views
app_name = 'story_9'

urlpatterns = [
    path('', views.view_login, name='index'),
    path('register/', views.register, name='register'),
    path('landing/', views.landing, name='landing'),
    path('landing/getData/', views.getData, name='getData'),
    path('logout/', views.view_logout, name='logout'),
]

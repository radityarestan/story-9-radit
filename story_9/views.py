from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from .forms import RegisterForm
from django.utils.safestring import mark_safe
from django.http import HttpResponse
import requests

# Create your views here.
def view_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return render(request, 'landing.html')

        else :
            messages.error(request,'*username or password not correct')
    return render(request, 'login.html')

def register(request):
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Congratulations, your account has been successfully created! Please Kindly Signin on the Link Above!')
        else :
            messages.error(request, mark_safe('*user registration failed'))

    else :
        form = RegisterForm()
    return render(request, 'register.html', {'form' : form})

def landing(request):
    return render(request, 'landing.html')

def getData(request):
    apikey = 'AIzaSyDvHEqdS5pvr3pIgLylXZP5AnZfYw7A4-Y'
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + request.POST['bookName'] + '&maxResults=40&key=' + apikey
    response = requests.get(url)
    if (response.status_code == 200) :
        return HttpResponse(response)

def view_logout(request):
    logout(request)
    return redirect('/')
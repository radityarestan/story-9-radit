from django.contrib.auth.forms import UserCreationForm, forms
from django.contrib.auth.models import User

class RegisterForm(UserCreationForm):
    class Meta:
        model = User
        fields = [
            'username',
            'password1',
            'password2',
        ]
    def __init__(self, *args, **kwargs):
        super(RegisterForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.update({'class' : 'input-field', 'placeholder' : 'Type your username', 'autocomplete' : 'off'})
        self.fields['password1'].widget.attrs.update({'class' : 'input-field', 'placeholder' : 'Type your password'})
        self.fields['password2'].widget.attrs.update({'class' : 'input-field', 'placeholder' : 'Confirm your password'})